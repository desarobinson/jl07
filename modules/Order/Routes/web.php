<?php

$current_hostname = app(Hyn\Tenancy\Contracts\CurrentHostname::class);

if($current_hostname) {
    Route::domain($current_hostname->fqdn)->group(function () {

        Route::get('order-forms/print/{external_id}/{format?}', 'OrderFormController@toPrint');
        Route::get('order-notes/print/{external_id}/{format?}', 'OrderNoteController@toPrint');

        Route::middleware(['auth', 'locked.tenant'])->group(function () {

            Route::prefix('order-notes')->group(function () {

                Route::get('/', 'OrderNoteController@index')->name('tenant.order_notes.index')->middleware(['redirect.level']);
                Route::get('columns', 'OrderNoteController@columns');
                Route::get('records', 'OrderNoteController@records');
                Route::get('create', 'OrderNoteController@create')->name('tenant.order_notes.create')->middleware(['redirect.level']);
                Route::get('edit/{id}', 'OrderNoteController@edit')->middleware(['redirect.level']);

                Route::get('tables', 'OrderNoteController@tables');
                Route::get('table/{table}', 'OrderNoteController@table');
                Route::post('/', 'OrderNoteController@store');
                Route::post('update', 'OrderNoteController@update');
                Route::get('record/{quotation}', 'OrderNoteController@record');
                Route::get('voided/{id}', 'OrderNoteController@voided');
                Route::get('item/tables', 'OrderNoteController@item_tables');
                Route::get('option/tables', 'OrderNoteController@option_tables');
                Route::get('search/customers', 'OrderNoteController@searchCustomers');
                Route::get('search/customer/{id}', 'OrderNoteController@searchCustomerById');
                Route::get('download/{external_id}/{format?}', 'OrderNoteController@download');
                // Route::get('print/{external_id}/{format?}', 'OrderNoteController@toPrint');
                Route::post('email', 'OrderNoteController@email');
                Route::post('duplicate', 'OrderNoteController@duplicate');
                Route::post('limpiar', 'OrderNoteController@limpiar');
                Route::get('record2/{quotation}', 'OrderNoteController@record2');
                Route::delete('destroy_order_note_item/{order_note_item}', 'OrderNoteController@destroy_order_note_item');
                Route::get('documents', 'OrderNoteController@documents');
                Route::post('documents', 'OrderNoteController@generateDocuments');
                Route::get('document_tables', 'OrderNoteController@document_tables');

            });

            Route::prefix('order-forms')->group(function () {

                Route::get('/', 'OrderFormController@index')->name('tenant.order_forms.index');
                Route::get('columns', 'OrderFormController@columns');
                Route::get('records', 'OrderFormController@records');
                Route::get('create/{id?}', 'OrderFormController@create')->name('tenant.order_forms.create');

                Route::post('tables', 'OrderFormController@tables');
                Route::get('table/{table}', 'OrderFormController@table');
                Route::post('/', 'OrderFormController@store');
                Route::get('record/{id}', 'OrderFormController@record');
                Route::get('item/tables', 'OrderFormController@item_tables');
                Route::get('option/tables', 'OrderFormController@option_tables');
                Route::get('search/customers', 'OrderFormController@searchCustomers');
                Route::get('search/customer/{id}', 'OrderFormController@searchCustomerById');
                Route::get('download/{external_id}/{format?}', 'OrderFormController@download');
                Route::post('email', 'OrderFormController@email');

                Route::get('dispatch-create/{id?}', 'OrderFormController@dispatchCreate');

            });

            Route::prefix('drivers')->group(function () {

                Route::get('/', 'DriverController@index')->name('tenant.order_forms.drivers.index');
                Route::get('columns', 'DriverController@columns');
                Route::get('records', 'DriverController@records');
                Route::get('record/{id}', 'DriverController@record');
                Route::get('tables', 'DriverController@tables');
                Route::post('/', 'DriverController@store');
                Route::delete('/{id}', 'DriverController@destroy');

            });

            Route::prefix('gviajes')->group(function () {

                Route::get('/', 'GviajeController@index')->name('tenant.order_forms.gviajes.index');
                Route::get('columns', 'GviajeController@columns');
                Route::get('records', 'GviajeController@records');
                Route::get('record/{id}', 'GviajeController@record');
                Route::get('tables', 'GviajeController@tables');
                Route::post('/', 'GviajeController@store');
                Route::delete('/{id}', 'GviajeController@destroy');

            });
            Route::prefix('vehiculos')->group(function () {

                Route::get('/', 'VehiculoController@index')->name('tenant.order_forms.vehiculos.index');
                Route::get('columns', 'VehiculoController@columns');
                Route::get('records', 'VehiculoController@records');
                Route::get('record/{id}', 'VehiculoController@record');
                Route::get('tables', 'VehiculoController@tables');
                Route::post('/', 'VehiculoController@store');
                Route::delete('/{id}', 'VehiculoController@destroy');

            });
            Route::prefix('carretas')->group(function () {

                Route::get('/', 'CarretaController@index')->name('tenant.order_forms.carretas.index');
                Route::get('columns', 'CarretaController@columns');
                Route::get('records', 'CarretaController@records');
                Route::get('record/{id}', 'CarretaController@record');
                Route::get('tables', 'CarretaController@tables');
                Route::post('/', 'CarretaController@store');
                Route::delete('/{id}', 'CarretaController@destroy');

            });
            Route::prefix('tractos')->group(function () {
                
                Route::get('/', 'TractoController@index')->name('tenant.order_forms.tractos.index');
                Route::get('columns', 'TractoController@columns');
                Route::get('records', 'TractoController@records');
                Route::post('upload', 'TractoController@upload');
                Route::post('upload1', 'TractoController@upload1');
                Route::post('upload2', 'TractoController@upload2');
                Route::get('record/{id}', 'TractoController@record');
                Route::get('tables', 'TractoController@tables');
                Route::post('/', 'TractoController@store');
                Route::delete('/{id}', 'TractoController@destroy');

            });
            Route::prefix('carretatractos')->group(function () {
                
                Route::get('/', 'CarretatractoController@index')->name('tenant.order_forms.carretatractos.index');
                Route::get('columns', 'CarretatractoController@columns');
                Route::get('records', 'CarretatractoController@records');
                Route::post('upload', 'CarretatractoController@upload');
                Route::post('upload1', 'CarretatractoController@upload1');
                Route::post('upload2', 'CarretatractoController@upload2');
                Route::get('record/{id}', 'CarretatractoController@record');
                Route::get('tables', 'CarretatractoController@tables');
                Route::post('/', 'CarretatractoController@store');
                Route::delete('/{id}', 'CarretatractoController@destroy');

            });
            Route::prefix('rutas')->group(function () {

                Route::get('/', 'RutaController@index')->name('tenant.order_forms.rutas.index');
                Route::get('columns', 'RutaController@columns');
                Route::get('records', 'RutaController@records');
                Route::get('record/{id}', 'RutaController@record');
                Route::get('tables', 'RutaController@tables');
                Route::post('/', 'RutaController@store');
                Route::delete('/{id}', 'RutaController@destroy');

            });
            Route::prefix('servicios')->group(function () {

                Route::get('/', 'ServicioController@index')->name('tenant.order_forms.servicios.index');
                Route::get('columns', 'ServicioController@columns');
                Route::get('records', 'ServicioController@records');
                Route::get('record/{id}', 'ServicioController@record');
                Route::get('tables', 'ServicioController@tables');
                Route::post('/', 'ServicioController@store');
                Route::delete('/{id}', 'ServicioController@destroy');

            });
            Route::prefix('gastosviajes')->group(function () {

                Route::get('/records', 'GastosviajeController@records');
                Route::get('/record/{id}', 'GastosviajeController@record');
                Route::post('', 'GastosviajeController@store');
                Route::delete('/{id}', 'GastosviajeController@destroy');

            });
            Route::prefix('dispatchers')->group(function () {

                Route::get('/', 'DispatcherController@index')->name('tenant.order_forms.dispatchers.index');
                Route::get('columns', 'DispatcherController@columns');
                Route::get('records', 'DispatcherController@records');
                Route::get('record/{id}', 'DispatcherController@record');
                Route::get('tables', 'DispatcherController@tables');
                Route::post('/', 'DispatcherController@store');
                Route::delete('/{id}', 'DispatcherController@destroy');

            });

        });
    });
}
