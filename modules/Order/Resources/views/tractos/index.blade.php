@extends('tenant.layouts.app')

@section('content')

    <tenant-tractos-index :type-user="{{json_encode(Auth::user()->type)}}"></tenant-tractos-index>

@endsection
