<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CarretatractoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'tarjetap' => $row->tarjetap,
                'tarjetac' => $row->tarjetac,                
                'soat' => $row->soat,                
                'revisiont' => $row->revisiont,
                'bonificacion' => $row->bonificacion,
                'alarma' => $row->alarma,
                'conos' => $row->conos,
                'tacos' => $row->tacos,
                'toldera' => $row->toldera,
                'plastico' => $row->plastico,
                'soga' => $row->soga, 
                'malla' => $row->malla,  
                'neumatico' => $row->neumatico,  
                'palosc' => $row->palosc,                
                'palosa' => $row->palosa,  
                'latas' => $row->latas,  
                'extintort' => $row->extintort,  
                'extintorc' => $row->extintorc,  
                'fajas' => $row->fajas,  
                'ratchet' => $row->ratchet,  
                'gancho' => $row->gancho,  
                'portallanta' => $row->portallanta,  
                'portapalos' => $row->portapalos,  
                'lucesd' => $row->lucesd,  
                'lucesi' => $row->lucesi, 
                'lucesp' => $row->lucesp,  
                'lucesint' => $row->lucesint, 
                'lucesre' => $row->lucesre, 
                'lucesfre' => $row->lucesfre, 
                'neblineros' => $row->neblineros, 
                'vigia' => $row->vigia, 
                'aire' => $row->aire, 
                'llanta1' => $row->llanta1,
                'llanta2' => $row->llanta2,
                'llanta3' => $row->llanta3,
                'llanta4' => $row->llanta4,
                'llanta5' => $row->llanta5,
                'llanta6' => $row->llanta6,

                'llanta7' => $row->llanta7,
                'llanta8' => $row->llanta8,
                'llanta9' => $row->llanta9,
                'llanta10' => $row->llanta10,
                'llanta11' => $row->llanta11,
                'llanta12' => $row->llanta12, 
                'llanta13' => $row->llanta13, 
                'llanta14' => $row->llanta14, 
                
                'imagen1' => $row->imagen1,
                'imagen2' => $row->imagen2,
                'imagen3' => $row->imagen3,
                'fecha' => $row->fecha,
                'desc1' => $row->desc1,
                'desc2' => $row->desc2,
                'desc3' => $row->desc3,
                'observaciones' => $row->observaciones,
                'placa' => $row->placa,
                'responsable' => $row->responsable,
                'establishment_id' => $row->establishment_id,
                'filename' => $row->filename,
                'external_id' => $row->external_id,
                'image_url' => ($row->imagen1 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$row->imagen1) : asset("/logo/{$row->imagen1}"),
                'image_url1' => ($row->imagen2 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$row->imagen2) : asset("/logo/{$row->imagen2}"),
                'image_url2' => ($row->imagen3 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$row->imagen3) : asset("/logo/{$row->imagen3}"),
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
            ];
        });
    }
}
