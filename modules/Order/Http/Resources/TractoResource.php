<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TractoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'conductor' => $this->conductor,
            'placa' => $this->placa,
            'fecha' => $this->fecha,
            'responsable' => $this->responsable,
            'tarjetap' => $this->tarjetap,
            'tarjetac' => $this->tarjetac,
            'soat' => $this->soat,
            'bonificacion' => $this->bonificacion,
            'alarma' => $this->alarma,
            'botiquin' => $this->botiquin,
            'elementos' => $this->elementos,
            'gata' => $this->gata,
            'llaves' => $this->llaves,
            'autoradio' => $this->autoradio,
            'nagua' => $this->nagua,
            'naceite' => $this->naceite,
            'luces' => $this->luces,
            'frenos' => $this->frenos,
            'embrague' => $this->embrague,
            'aguaaceite' => $this->aguaaceite,
            'tacografo' => $this->tacografo,
            'neblineros' => $this->neblineros,
            'vigia' => $this->vigia,
            'aire' => $this->aire,
            'llanta1' => $this->llanta1,
            'llanta2' => $this->llanta2,
            'llanta3' => $this->llanta3,
            'llanta4' => $this->llanta4,

            'llanta5' => $this->llanta5,
            'llanta6' => $this->llanta6,
            'llanta7' => $this->llanta7,
            'llanta8' => $this->llanta8,
            'llanta9' => $this->llanta9,
            'llanta10' => $this->llanta10,
            'desc01' => $this->desc01,
            'desc02' => $this->desc02,
            'desc03' => $this->desc03,
            'combustibleactual' => $this->combustibleactual,
            'combustiblentrega' => $this->combustiblentrega,
            'observaciones' => $this->observaciones,
            'filename' => $this->filename,
            'external_id' => $this->external_id,
            'km' => $this->km,
            'image_url1' => ($this->imagen2 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$this->imagen2) : asset("/logo/{$this->imagen2}"),
            'image_url2' => ($this->imagen3 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$this->imagen3) : asset("/logo/{$this->imagen3}"),
            'image_url' => ($this->imagen1 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$this->imagen1) : asset("/logo/{$this->imagen1}"),
            

        ];
    }
}
