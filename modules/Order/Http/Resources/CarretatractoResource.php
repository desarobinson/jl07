<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CarretatractoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tarjetap' => $this->tarjetap,
            'tarjetac' => $this->tarjetac,
            'soat' => $this->soat,
            'revisiont' => $this->revisiont,
            'bonificacion' => $this->bonificacion,
            'alarma' => $this->alarma,
            'conos' => $this->conos,
            'tacos' => $this->tacos,
            'toldera' => $this->toldera,
            'plastico' => $this->plastico,
            'soga' => $this->soga,
            'malla' => $this->malla,
            'neumatico' => $this->neumatico,
            'palosc' => $this->palosc,
            'palosa' => $this->palosa,
            'latas' => $this->latas,
            'extintort' => $this->extintort,
            'extintorc' => $this->extintorc,
            'fajas' => $this->fajas,
            'ratchet' => $this->ratchet,
            'gancho' => $this->gancho,
            'portallanta' => $this->portallanta,
            'portapalos' => $this->portapalos,
            'lucesd' => $this->lucesd,
            'lucesi' => $this->lucesi,
            'lucesp' => $this->lucesp,
            'lucesint' => $this->lucesint,
            'lucesre' => $this->lucesre,

            'lucesfre' => $this->lucesfre,
            'neblineros' => $this->neblineros,
            'vigia' => $this->vigia,
            'aire' => $this->aire,
            'llanta1' => $this->llanta1,
            'llanta2' => $this->llanta2,
            'llanta3' => $this->llanta3,

            'llanta4' => $this->llanta4,
            'llanta5' => $this->llanta5,
            'llanta6' => $this->llanta6,
            'llanta7' => $this->llanta7,
            'llanta8' => $this->llanta8,
            'llanta9' => $this->llanta9,
            'llanta10' => $this->llanta10,
            'llanta11' => $this->llanta11,
            'llanta12' => $this->llanta12,
            'llanta13' => $this->llanta13,
            'llanta14' => $this->llanta14,
            'observaciones' => $this->observaciones,
            'imagen1' => $this->imagen1,
            'imagen2' => $this->imagen2,
            'imagen3' => $this->imagen3,
            'fecha' => $this->fecha,
            'filename' => $this->filename,
            'desc1' => $this->desc1,
            'desc2' => $this->desc2,
            'desc3' => $this->desc3,
            'placa' => $this->placa,
            'external_id' => $this->external_id,
            'responsable' => $this->responsable,
            'establishment_id' => $this->establishment_id,
            'image_url1' => ($this->imagen2 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$this->imagen2) : asset("/logo/{$this->imagen2}"),
            'image_url2' => ($this->imagen3 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$this->imagen3) : asset("/logo/{$this->imagen3}"),
            'image_url' => ($this->imagen1 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$this->imagen1) : asset("/logo/{$this->imagen1}"),
            

        ];
    }
}
