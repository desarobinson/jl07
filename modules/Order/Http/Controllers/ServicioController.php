<?php
namespace Modules\Order\Http\Controllers;

use Modules\Order\Http\Requests\ServicioRequest;
use Modules\Order\Http\Resources\ServicioCollection;
use Modules\Order\Http\Resources\ServicioResource;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Http\Controllers\Controller;
use Modules\Order\Models\Servicio;
use Illuminate\Http\Request;

class ServicioController extends Controller
{

    public function index()
    {
        return view('order::servicios.index');
    }

    public function columns()
    {
        return [
            'nombre' => 'Nombre',
            
        ];
    }

    public function records(Request $request)
    {

        $records = Servicio::where($request->column, 'like', "%{$request->value}%")
                            ->orderBy('nombre');

        return new ServicioCollection($records->paginate(config('tenant.items_per_page')));
    }


    public function tables()
    {
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $api_service_token = config('configuration.api_service_token');

        return compact('identity_document_types', 'api_service_token');
    }

    public function record($id)
    {
        $record = new ServicioResource(Servicio::findOrFail($id));

        return $record;
    }

    public function store(ServicioRequest $request)
    {

        $id = $request->input('id');
        $record = Servicio::firstOrNew(['id' => $id]);
        $record->fill($request->all());
        $record->save();

        return [
            'success' => true,
            'message' => ($id)?'Servicio editado con éxito':'Servicio registrado con éxito',
            'id' => $record->id
        ];
    }

    public function destroy($id)
    {

        $record = Servicio::findOrFail($id);
        $record->delete();

        return [
            'success' => true,
            'message' => 'Servicio eliminado con éxito'
        ];

    }

}
