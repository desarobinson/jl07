@php
    $establishment = $document->establishment;
    
    $tittle = $document->series.'-'.str_pad($document->number, 8, '0', STR_PAD_LEFT);
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    {{--<link href="{{ $path_style }}" rel="stylesheet" />--}}
</head>
<body>
<table class="full-width">
    <tr>
        @if($company->logo)
            <td width="20%">
                <div class="company_logo_box">
                    <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo" style="max-width: 150px;">
                </div>
            </td>
        @else
            <td width="20%">
                {{--<img src="{{ asset('logo/logo.jpg') }}" class="company_logo" style="max-width: 150px">--}}
            </td>
        @endif
        <td width="50%" class="pl-3">
            <div class="text-left">
                <h4 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>
               

            </div>
        </td>
        <td width="20%" class="pl-3">
            <div class="text-left">
            <h3 class="text-center">Numero : 0000{{ $document->id}}</h3>
               

            </div>
        </td>
        
    </tr>
</table>
<table class="full-width" border="1">
    <tr>      
      
        <td width="50%" class="pl-3">
       <b> 1.CONDUCTOR : </b>{{ $document->conductor}}
        </td>
        <td width="50%" class="pl-3">
        <b>FECHA: </b> {{ $document->fecha}}
        </td>
        
    </tr>
    <tr>      
      
      <td width="50%" class="pl-3">
      <b> 2.PLACA TRACTO  :</b> {{ $document->placa}}   <b>Km :</b> {{ $document->km}} 
      </td>
      <td width="50%" class="pl-3">
      <b>RESPONSABLE :</b> {{ $document->responsable}}
      </td>
      
  </tr>
   
</table>

<hr>
<table class="full-width" border="1">
    <tr>      
      
        <td width="30%" class="pl-3">
        <b> 3. DOCUMENTACION TRACTO  </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        <td width="30%" class="pl-3">
        <b> 4. ARTICULOS EN TRACTO  </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        
    </tr>
    <tr>      
      
        <td width="30%" class="pl-3">
        Tarjeta de Propiedad  
        </td>
        <td width="5%" class="pl-3">
        
        @if ($document->tarjetap == '1')
            <span>X</span>    
         @endif
         
        </td>
        <td width="5%" class="pl-3">
        @if ($document->tarjetap == '2')
            <span>X</span>    
         @endif
        </td>
        <td width="30%" class="pl-3">
        Alarma Retroceso  
        </td>
        <td width="5%" class="pl-3">
        @if ($document->alarma == '1')
            <span>X</span>    
         @endif
        </td>
        <td width="5%" class="pl-3">
        @if ($document->alarma == '2')
            <span>X</span>    
         @endif 
        </td>
        
    </tr>
    <tr>      
      
      <td width="30%" class="pl-3">
      Tarjeta de Circulacion 
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->tarjetac == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->tarjetac == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Botiquin
      </td>
      <td width="5%" class="pl-3">
      @if ($document->botiquin == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->botiquin == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      SOAT
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->soat == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->soat == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Elementos de Seguridad.
      </td>
      <td width="5%" class="pl-3">
      @if ($document->elementos == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->elementos == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Revision Tecnica
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->revision == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->revision == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Gata
      </td>
      <td width="5%" class="pl-3">
      @if ($document->gata == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->gata == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Bonificacion
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->bonificacion == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->bonificacion == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Llaves
      </td>
      <td width="5%" class="pl-3">
      @if ($document->llaves == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->llaves == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Formatos
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->formatos == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->formatos == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Autoradio
      </td>
      <td width="5%" class="pl-3">
      @if ($document->autoradio == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->autoradio == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
</table>
<hr>
<table class="full-width" border="1">
    <tr>      
      
        <td width="30%" class="pl-3">
        <b> 5. EQUIPO  </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        <td width="30%" class="pl-3">
        <b> 6. NEUMATICOS </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        
    </tr>
    <tr>      
      
        <td width="30%" class="pl-3">
        Nivel de agua dentro rango
        </td>
        <td width="5%" class="pl-3">
        
        @if ($document->nagua == '1')
            <span>X</span>    
         @endif
         
        </td>
        <td width="5%" class="pl-3">
        @if ($document->nagua == '2')
            <span>X</span>    
         @endif
        </td>
        <td width="30%" class="pl-3">
        Vigia en buene estado  
        </td>
        <td width="5%" class="pl-3">
        @if ($document->vigia == '1')
            <span>X</span>    
         @endif
        </td>
        <td width="5%" class="pl-3">
        @if ($document->vigia == '2')
            <span>X</span>    
         @endif 
        </td>
        
    </tr>
    <tr>      
      
      <td width="30%" class="pl-3">
      Nivel de aceite, dentro rango
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->naceite == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->naceite == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Perdida de aire
      </td>
      <td width="5%" class="pl-3">
      @if ($document->aire == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->aire == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      SOAT
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->soat == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->soat == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Luces operativas
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->luces == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->luces == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Frenos operativos

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->frenos == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->frenos == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Embrague
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->embrague == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->embrague == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  Fugas y Filtraciones
  <tr>      
      
      <td width="30%" class="pl-3">
      Agua/aceite/combustible

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->aguaaceite == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->aguaaceite == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Tacografo operativo

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->tacografo == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->tacografo == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Neblineros

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->neblineros == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->neblineros == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
</table>
<hr>
<table class="full-width" border="1">
    <tr>    
    <td width="30%" class="pl-3">
    Combustible Actual : {{ $document->combustibleactual}} <br>
    Combustible Entrega : {{ $document->combustiblentrega}}
    </td>
    <td width="20%" class="pl-3"> 
    <br>
   
    BUENO <b>B</b><br>
    REGULAR <b>R</b><br>
    MALO <b>M</b>
    </td>
    <td width="50%" class="pl-3">
    
    <b>NEUMATICOS</b><br>
    <table class="full-width" border="1">
        <tr>    
        <td width="30%" class="pl-3">.</td>
        <td width="30%" class="pl-3">{{ $document->llanta3}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta7}}</td>
        </tr>
        <tr>    
        <td width="30%" class="pl-3">{{ $document->llanta1}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta4}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta8}}</td>
        </tr>
        <tr>    
        <td width="30%" class="pl-3">{{ $document->llanta2}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta5}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta9}}</td>
        </tr>
        <tr>    
        <td width="30%" class="pl-3">.</td>
        <td width="30%" class="pl-3">{{ $document->llanta6}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta10}}</td>
        </tr>
    </table>
    </td>
    </tr>
</table>
<hr>
<table class="full-width" border="1">
    <tr>    
    <td width="100%" class="pl-3">
    Observaciones del Tracto: {{ $document->observaciones}}
    </td>
    </tr>
    </table>
    <hr>
    <table class="full-width" border="1">
    <tr>
    <td width="100%" class="pl-3">
    <br>
    Descripción imagen : {{ $document->desc01}} <br>
    <br>
                <div >
                    <img src="data:{{mime_content_type(public_path("storage/uploads/items/{$document->imagen1}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/items/{$document->imagen1}")))}}" alt="{{$document->imagen1}}"  style="max-width: 500px;">
                </div>
    </td>       
    </tr> 
    <tr>
    <td width="100%" class="pl-3">
    <br>
    Descripción imagen : {{ $document->desc02}} <br>
    <br>
                <div >
                    <img src="data:{{mime_content_type(public_path("storage/uploads/items/{$document->imagen2}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/items/{$document->imagen2}")))}}" alt="{{$document->imagen2}}"  style="max-width: 500px;">
                </div>
                </td>       
    </tr> 
    <tr>
    <td width="100%" class="pl-3">
    <br>
    Descripción imagen : {{ $document->desc03}} <br>
    <br>
                <div >
                    <img src="data:{{mime_content_type(public_path("storage/uploads/items/{$document->imagen3}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/items/{$document->imagen3}")))}}" alt="{{$document->imagen3}}"  style="max-width: 500px;">
                </div>
                </td>       
    </tr> 
    </table>           

</body>
</html>
